import { Request, Response } from "express";
import { Response as Resp } from "../models/response";
import CategoryService from "../services/category.service";

const categoryService: CategoryService = new CategoryService();
export default class CategoryController {
  public static getCategories(
    req: Request,
    res: Response
  ) /*: Promise<Response | void>*/ {
    try {
      let categories: any = categoryService.getAllCategories();
      return res.json(categories);
    } catch (e) {
      console.log(e);
    }
  }

  public static getCategory(
    req: Request,
    res: Response
  ) /*: Promise<Response | void>*/ {
    try {
      const id = req.params.id;
      const type = req.params.type;
      let categories: any = categoryService.getCategoryById(parseInt(id), type);
      return res.json(categories);
    } catch (e) {
      console.log(e);
    }
  }

  public static postCategory(
    req: Request,
    res: Response
  ) /*: Promise<Response | void>*/ {
    try {
      const newPost: any = req.body;
      return res.json(categoryService.addCategory(newPost));
    } catch (e) {
      console.log(e);
    }
  }

  public static putCategory(req: Request, res: Response) {
    try {
      const updatePost: any = req.body;
      const id: any = req.params.id;
      return res.json(categoryService.updateCategory(updatePost, parseInt(id)));
    } catch (e) {
      console.log(e);
    }
  }

  public static deleteCategory(req: Request, res: Response) {
    try {
      const id = req.params.id;
      return res.json(categoryService.deleteCategory(parseInt(id)));
    } catch (e) {
      console.log(e);
    }
  }

  public static getCategoriesByType(req: Request, res: Response) {
    try {
      const type = req.params.type;
      let categories: any =categoryService.getCategoruesByType(type);
      return res.json(categories);
    } catch (e) {
        console.log(e);
    }
  }

}
/*
export function indexWelcome(req: Request, res: Response): Response {
    return res.json('Welcome to the Api'); 
}*/
