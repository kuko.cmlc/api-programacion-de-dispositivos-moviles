import { Request, Response } from 'express';
import talesMock from '../mock-data/talesMock';
import { Response as Resp } from '../models/response';
import TaleService from '../services/tale.service';

const taleService: TaleService = new TaleService();
export default class TaleController {

    public static getTales(req: Request, res: Response){
        try {
            let tales: any = taleService.getAllTales();
            return res.json(tales);
        }
        catch (e) {
            console.log(e)
        }
    }

    public static getTale(req: Request, res: Response){
        try {
            const id = req.params.id;
            let tales: any = taleService.getTaleById(parseInt(id));
            let response = new Resp('data', tales);
            return res.json(response);
        }
        catch (e) {
            console.log(e)
        }
    }

    // public static postTale(req: Request, res: Response){
    //     try {
    //         const newTale: any = req.body;
    //         console.log(req.body);
    //         return res.json(taleService.addTale(newTale));
    //     }
    //     catch (e) {
    //         console.log(e)
    //     }
    // }

    // public static putTale(req: Request, res: Response){
    //     try {
    //         const updateTale: any = req.body;
    //         const id: any = req.params.id;
    //         console.log(req.body);
    //         return res.json(taleService.updateTale(updateTale, parseInt(id)));
    //     }
    //     catch (e) {
    //         console.log(e)
    //     }
    // }

    // public static deleteTale(req: Request, res: Response){
    //     try {
    //         const id = req.params.id;
    //         return res.json(taleService.deleteTale(parseInt(id)));
    //     }
    //     catch (e) {
    //         console.log(e)
    //     }
    // }

}