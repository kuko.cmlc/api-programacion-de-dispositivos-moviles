import { Request, Response } from 'express';
import { Response as Resp } from '../models/response';
import UserService from '../services/user.service';

const userService: UserService = new UserService();
export default class userController {

    public static async getAllUsers(req: Request, res: Response){
        try {
            let users: any = await userService.getAllUsers();
            let response = new Resp('data', users);
            return res.json(users);
        }
        catch (e) {
            console.log(e)
        }
    }

    public static async registerNewUser(req: Request, res: Response){
        try {
            const newUser: any = req.body;
            let response: any = await userService.registerNewUser(newUser);
            return res.json(response);
        }
        catch (e) {
            console.log(e)
        }
    }

    public static async serachUserByEmail(req: Request, res: Response){
      try {
          const user: any = req.body;
          let users: any = await userService.searchUserByEmail(user);
          return res.json(users);
      }
      catch (e) {
          console.log(e)
      }
  }
    // public static postTale(req: Request, res: Response){
    //     try {
    //         const newTale: any = req.body;
    //         console.log(req.body);
    //         return res.json(taleService.addTale(newTale));
    //     }
    //     catch (e) {
    //         console.log(e)
    //     }
    // }

    // public static putTale(req: Request, res: Response){
    //     try {
    //         const updateTale: any = req.body;
    //         const id: any = req.params.id;
    //         console.log(req.body);
    //         return res.json(taleService.updateTale(updateTale, parseInt(id)));
    //     }
    //     catch (e) {
    //         console.log(e)
    //     }
    // }

    // public static deleteTale(req: Request, res: Response){
    //     try {
    //         const id = req.params.id;
    //         return res.json(taleService.deleteTale(parseInt(id)));
    //     }
    //     catch (e) {
    //         console.log(e)
    //     }
    // }

}