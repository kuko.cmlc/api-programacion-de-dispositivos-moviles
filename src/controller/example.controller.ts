import { Request, Response } from 'express';
import { Response as Resp } from '../models/response';
import ExampleService from '../services/example.service';

const exampleService: ExampleService = new ExampleService();
export default class ExampleController {

    public static getExamples(req: Request, res: Response){
        try {
            let examples: any = exampleService.getAllExamples()
            let response = new Resp('data', examples);
            return res.json(response);
        }
        catch (e) {
            console.log(e)
        }
    }

    public static getExample(req: Request, res: Response){
        try {
            const id = req.params.id;
            let example: any = exampleService.getExampleById(parseInt(id));
            return res.json(example);
        }
        catch (e) {
            console.log(e)
        }
    }

    public static getExampleByCategory(req: Request, res: Response){
        try {
            const id = req.params.id;
            console.log(id);
            let examples: any = exampleService.getExamplesByCategory(parseInt(id));
            return res.json(examples);
        }
        catch (e) {
            console.log(e)
        }
    }

}