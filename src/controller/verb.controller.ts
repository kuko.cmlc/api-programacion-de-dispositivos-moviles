import { Request, Response } from 'express';
import { Response as Resp } from '../models/response';
import VerbService from '../services/verb.service';

const verbService: VerbService = new VerbService();
export default class VerbController {
    
    public static getVerbs(req: Request, res: Response){
        try {
            let verbs: any = verbService.getAllVerbs();
            let response = new Resp('data', verbs);
            return res.json(response);
        }
        catch (e) {
            console.log(e)
        }
    }

    public static getVerb(req: Request, res: Response){
        try {
            const id = req.params.id;
            let verbs: any = verbService.getVerbById(parseInt(id));
            let response = new Resp('data', verbs);
            return res.json(response);
        }
        catch (e) {
            console.log(e)
        }
    }

    public static getVerbsByTime(req: Request, res: Response){
        try {
            const time = req.params.id;
            let verbs: any = verbService.getVerbsByTime(time);
            let response = new Resp('data', verbs);
            return res.json(response);
        }
        catch (e) {
            console.log(e)
        }
    }

    // public static postVerb(req: Request, res: Response){
    //     try {
    //         const newVerb: any = req.body;
    //         console.log(req.body);
    //         return res.json(verbService.addVerb(newVerb));
    //     }
    //     catch (e) {
    //         console.log(e)
    //     }
    // }

    // public static putVerb(req: Request, res: Response){
    //     try {
    //         const updateVerb: any = req.body;
    //         const id: any = req.params.id;
    //         console.log(req.body);
    //         return res.json(verbService.updateVerb(updateVerb, parseInt(id)));
    //     }
    //     catch (e) {
    //         console.log(e)
    //     }
    // }

    // public static deleteVerb(req: Request, res: Response){
    //     try {
    //         const id = req.params.id;
    //         return res.json(verbService.deleteVerb(parseInt(id)));
    //     }
    //     catch (e) {
    //         console.log(e)
    //     }
    // }

}