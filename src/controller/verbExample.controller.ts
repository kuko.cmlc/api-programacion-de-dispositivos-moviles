import { Request, Response } from 'express';
import { Response as Resp } from '../models/response';
import VerbExampleService from '../services/verbExample.service';

const verbExampleService: VerbExampleService = new VerbExampleService();
export default class VerbExampleController {

    public static getVerbExamples(req: Request, res: Response){
        try {
            let verbExamples: any = verbExampleService.getAllVerbExamples();
            let response = new Resp('data', verbExamples);
            return res.json(response);
        }
        catch (e) {
            console.log(e)
        }
    }

    public static getAllVerbExamplesFromAVerb(req: Request, res: Response){
        try {
            const id = req.params.id;
            let verbExamples: any = verbExampleService.getAllVerbExamplesFromAVerb(parseInt(id));
            let response = new Resp('data', verbExamples);
            return res.json(response);
        }
        catch (e) {
            console.log(e)
        }
    }

    public static getVerbExampleById(req: Request, res: Response){
        try {
            const id = req.params.id;
            let verbExamples: any = verbExampleService.getVerbExampleById(parseInt(id));
            let response = new Resp('data', verbExamples);
            return res.json(response);
        }
        catch (e) {
            console.log(e)
        }
    }

}