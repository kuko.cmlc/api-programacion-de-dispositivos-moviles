import { Category } from "../models/category";


let categoriesMock: Array<Category> = new Array<Category>();

//PRONOMS
categoriesMock.push(new Category(1, "Ñuqa", "Yo", "pronoms"));
categoriesMock.push(new Category(2, "Qam", "Tu", "pronoms"));
categoriesMock.push(new Category(3, "Pay", "El/Ella", "pronoms"));
categoriesMock.push(new Category(4, "Ñuqanchik - Ñuqayku", "Nosotros", "pronoms"));
categoriesMock.push(new Category(5, "Qamkuna", "Ustedes", "pronoms"));
categoriesMock.push(new Category(6, "Paykuna", "Ellos", "pronoms"));

//GREETINGS
categoriesMock.push(new Category(7, "N/A", "Por el horario del día", "greetings"));
categoriesMock.push(new Category(8, "N/A", "Despedida", "greetings"));
categoriesMock.push(new Category(9, "N/A", "Informal", "greetings"));

//ANIMALS
categoriesMock.push(new Category(10, "N/A", "Terrestres", "animals"));
categoriesMock.push(new Category(11, "N/A", "Acuáticos", "animals"));
categoriesMock.push(new Category(12, "N/A", "Aéreos", "animals"));

//MANNERS
categoriesMock.push(new Category(13, "N/A", "Gracias", "manners"));
categoriesMock.push(new Category(14, "N/A", "Por favor", "manners"));

//FAMILY
categoriesMock.push(new Category(15, "N/A", "Cercanos", "family"));
categoriesMock.push(new Category(16, "N/A", "Lejanos", "family"));

//DATES
categoriesMock.push(new Category(17, "N/A", "Dias", "dates"));
categoriesMock.push(new Category(18, "N/A", "Meses", "dates"));

//FOOD
categoriesMock.push(new Category(19, "Yaku", "Agua", "food"));
categoriesMock.push(new Category(20, "Hilli", "Jugo", "food"));
categoriesMock.push(new Category(21, "Llilli", "Leche", "food"));
categoriesMock.push(new Category(22, "Puky q’allku", "Limón", "food"));
categoriesMock.push(new Category(23, "Aycha", "Carne", "food"));
categoriesMock.push(new Category(24, "Sara", "Maiz", "food"));
categoriesMock.push(new Category(25, "Kachi", "Sal", "food"));
categoriesMock.push(new Category(26, "Huevo", "Runtu", "food"));
categoriesMock.push(new Category(27, "Siwara", "Cebada", "food"));
categoriesMock.push(new Category(28, "Ch’uño", "Chuño", "food"));
categoriesMock.push(new Category(29, "Sanawurya", "Zanahoria", "food"));
categoriesMock.push(new Category(30, "Pilliyuyu", "Lechuga", "food"));
categoriesMock.push(new Category(31, "Chilltu", "Tomate", "food"));
categoriesMock.push(new Category(32, "Ajus ", "Ajo", "food"));
categoriesMock.push(new Category(33, "Uchu", "Ají", "food"));
categoriesMock.push(new Category(34, "Qora ", "Yerba", "food"));

//COLOR
categoriesMock.push(new Category(35, "Yana", "Negro", "colors"));
categoriesMock.push(new Category(36, "Yana anqas", "Azul oscuro", "colors"));
categoriesMock.push(new Category(37, "Anqas", "Azul", "colors"));
categoriesMock.push(new Category(38, "Yuraq anqas", "Celeste", "colors"));
categoriesMock.push(new Category(39, "Yana q'omer", "Verde Oscuro", "colors"));
categoriesMock.push(new Category(40, "Q'omer", "Verde", "colors"));
categoriesMock.push(new Category(41, "Q'ello", "Amarillo", "colors"));
categoriesMock.push(new Category(42, "Q'ello puka", "Anaranjado", "colors"));
categoriesMock.push(new Category(43, "Puka", "Rojo", "colors"));
categoriesMock.push(new Category(44, "Llankha", "Rosado", "colors"));
categoriesMock.push(new Category(45, "Pukapanti", "Color rosa", "colors"));
categoriesMock.push(new Category(46, "Ch'unpi", "Marrón", "colors"));

//NUMBERS
categoriesMock.push(new Category(47, "Ch’usaq", "Cero", "numbers"));
categoriesMock.push(new Category(48, "Huk", "Uno", "numbers"));
categoriesMock.push(new Category(49, "Iskay", "Dos", "numbers"));
categoriesMock.push(new Category(50, "Kimsa", "Tres", "numbers"));
categoriesMock.push(new Category(51, "Tawa", "Cuatro", "numbers"));
categoriesMock.push(new Category(52, "Pichqa", "Cinco", "numbers"));
categoriesMock.push(new Category(53, "Suqta", "Seis", "numbers"));
categoriesMock.push(new Category(54, "Qanchis", "Siete", "numbers"));
categoriesMock.push(new Category(55, "Pusqa", "Ocho", "numbers"));
categoriesMock.push(new Category(56, "Jisq'un", "Nueve", "numbers"));
categoriesMock.push(new Category(57, "Chunka", "Diez", "numbers"));
categoriesMock.push(new Category(58, "Pachak", "Cien", "numbers"));
categoriesMock.push(new Category(59, "Waranqa", "Mil", "numbers"));
categoriesMock.push(new Category(60, "Junu", "Millon", "numbers"));

export default categoriesMock;