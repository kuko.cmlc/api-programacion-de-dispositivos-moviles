import { Example } from "../models/example";


let examplesMock: Array<Example> = new Array<Example>();

//SINGLE EXAMPLES
//examples-pronoms
examplesMock.push(new Example(1, 1, "Ñuqa yachaywasi rini.", "Yo camino a la escuela.", "pronoms"));
examplesMock.push(new Example(2, 1, "Ñuqa takini.", "Yo canto.", "pronoms"));
examplesMock.push(new Example(3, 2, "Qam takinki.", "Tú cantas.", "pronoms"));
examplesMock.push(new Example(4, 3, "Payqa panaymi.", "Ella es mi hermana.", "pronoms"));
examplesMock.push(new Example(5, 3, "Pay takin.", "El canta.", "pronoms"));
examplesMock.push(new Example(6, 4, "Ñuqayku llank’ayku.", "Nosotros trabajamos.", "pronoms"));
examplesMock.push(new Example(7, 4, "Ñuqayku takiyku.", "Nosotros cantamos.", "pronoms"));
examplesMock.push(new Example(8, 5, "Qamkuna takinkichik.", "Ustedes cantan.", "pronoms"));
examplesMock.push(new Example(9, 6, "Paykuna payta mashkanku.", "Ellos buscan a ella.", "pronoms"));
examplesMock.push(new Example(10, 6, "Paykuna takinku.", "Ellos cantan.", "pronoms"));

//examples-saludos-por-horario
examplesMock.push(new Example(11, 7, "Allin paqarin", "Buenos días", "greetings"));
examplesMock.push(new Example(12, 7, "Allin p´unchaw", "Buen día", "greetings"));
examplesMock.push(new Example(13, 7, "Allin sukha", "Buenas tardes", "greetings"));
examplesMock.push(new Example(14, 7, "Allin ch´isi", "Buenas noches", "greetings"));
//examples-saludos-despedida
examplesMock.push(new Example(15, 8, "Waq kutikama", "Hasta luego", "greetings"));
examplesMock.push(new Example(16, 8, "Tinkunakama ", "Hasta otro encuentro", "greetings"));
examplesMock.push(new Example(17, 8, "Napaykuripuwanki ", "Me lo saludas", "greetings"));
examplesMock.push(new Example(18, 8, "Waq ratukaman", "Nos vemos luego", "greetings"));
examplesMock.push(new Example(19, 8, "Waq kutikama kachun", "Que sea hasta otra oportunidad", "greetings"));
examplesMock.push(new Example(20, 8, "Ripunay tian, tinkunakama", "Me tengo que ir, hasta luego", "greetings"));
//examples-saludos-informal
examplesMock.push(new Example(21, 9, "Imaynalla", "Cómo estás", "greetings"));
examplesMock.push(new Example(22, 9, "Walliqlla ", "Estoy bien", "greetings"));

//examples-animales-terrestres
examplesMock.push(new Example(23, 10, "Alqu", "Perro", "animals"));
examplesMock.push(new Example(24, 10, "Atuq", "Zorro", "animals"));
examplesMock.push(new Example(25, 10, "Waka", "Vaca", "animals"));
examplesMock.push(new Example(26, 10, "Turuu", "Toro", "animals"));
examplesMock.push(new Example(27, 10, "Kawallu", "Caballo", "animals"));
examplesMock.push(new Example(28, 10, "Uwija", "Oveja", "animals"));
examplesMock.push(new Example(29, 10, "Paqucha", "Alpaca", "animals"));
//examples-animales-acuaticos
examplesMock.push(new Example(30, 11, "Challwa", "pez", "animals"));
examplesMock.push(new Example(31, 11, "Charapa", "Tortuga", "animals"));
examplesMock.push(new Example(32, 11, "Kachu k'arachiq", "Medusa", "animals"));
examplesMock.push(new Example(33, 11, "Chinlus", "Sardina", "animals"));
examplesMock.push(new Example(34, 11, "Tiputun", "Tiburón", "animals"));
examplesMock.push(new Example(35, 11, "Puru Challwa", "Pez Globo", "animals"));
//examples-animales-aereos
examplesMock.push(new Example(36, 12, "K'alla", "Loro", "animals"));
examplesMock.push(new Example(37, 12, "Anka", "Águila", "animals"));
examplesMock.push(new Example(38, 12, "Ch'usiq", "Lechuza", "animals"));
examplesMock.push(new Example(39, 12, "Chuspi", "Mosca", "animals"));
examplesMock.push(new Example(40, 12, "Kuntur", "Cóndor", "animals"));
examplesMock.push(new Example(41, 12, "Lachiwa ", "Abeja", "animals"));

//examples-manners-gracias
examplesMock.push(new Example(42, 13, "X", "X", "manners"));
examplesMock.push(new Example(43, 13, "X", "X", "manners"));
//examples-manners-porfavor
examplesMock.push(new Example(44, 14, "X", "X", "manners"));
examplesMock.push(new Example(45, 14, "X", "X", "manners"));

//examples-familia-cercanos
examplesMock.push(new Example(46, 15, "qusa", "esposo", "family"));
examplesMock.push(new Example(47, 15, "mama", "madre", "family"));
examplesMock.push(new Example(48, 15, "tata", "padre", "family"));
examplesMock.push(new Example(49, 15, "ñaña", "hermana", "family"));
examplesMock.push(new Example(50, 15, "Wawqi", "hermano", "family"));
examplesMock.push(new Example(51, 15, "Churi", "hijo", "family"));
examplesMock.push(new Example(52, 15, "Usuri", "hija", "family"));
//examples-familia-lejanos
examplesMock.push(new Example(53, 16, "Qatay", "Yerno", "family"));
examplesMock.push(new Example(54, 16, "Qhachun", "Nuera", "family"));
examplesMock.push(new Example(55, 16, "Kisma", "Suegra (del varón)", "family"));
examplesMock.push(new Example(56, 16, "Aqe, kiwachi", "Suegra (de la mujer)", "family"));

//examples-dia
examplesMock.push(new Example(57, 17, "Killachay", "Lunes", "dates"));
examplesMock.push(new Example(58, 17, "Antichay", "Martes", "dates"));
examplesMock.push(new Example(59, 17, "Qoyllurchay", "Miércoles", "dates"));
examplesMock.push(new Example(60, 17, "Illapachay", "Jueves", "dates"));
examplesMock.push(new Example(61, 17, "Ch’askachay", "Viernes", "dates"));
examplesMock.push(new Example(62, 17, "Ch’askachay", "Sábado", "dates"));
examplesMock.push(new Example(63, 17, "Intichay", "Domingo", "dates"));

//examples-meses
examplesMock.push(new Example(64, 18, "Kamay Raymi killa", "Enero", "dates"));
examplesMock.push(new Example(65, 18, "Hatun Poqoy killa", "Febrero", "dates"));
examplesMock.push(new Example(66, 18, "Pawqar Waray killa", "Marzo", "dates"));
examplesMock.push(new Example(67, 18, "Ayriway killa", "Abril", "dates"));
examplesMock.push(new Example(68, 18, "Aymuray killa", "Mayo", "dates"));
examplesMock.push(new Example(69, 18, "Inti Raymi killa", "Junio", "dates"));
examplesMock.push(new Example(70, 18, "Anta Situwa killa", "Julio", "dates"));
examplesMock.push(new Example(71, 18, "Qhapaq Situwa killa", "Agosto", "dates"));
examplesMock.push(new Example(72, 18, "Unu Raymi killa", "Septiembre", "dates"));
examplesMock.push(new Example(73, 18, "Qoya Raymi killa", "Octubre", "dates"));
examplesMock.push(new Example(74, 18, "Ayamarq’a killa", "Noviembre", "dates"));
examplesMock.push(new Example(75, 18, "Qhapaq Raymi killa", "Diciembre", "dates"));

export default examplesMock;