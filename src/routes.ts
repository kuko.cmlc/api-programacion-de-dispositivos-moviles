import { Router } from 'express';
import CategoryController from './controller/category.controller';
import ExampleController from './controller/example.controller';
import TaleController from './controller/tale.controller';
import userController from './controller/user.controller';
import VerbController from './controller/verb.controller';
import VerbExampleController from './controller/verbExample.controller';
const router = Router();


router.get('/', CategoryController.getCategories);

/* CATEGORY */
router.get('/categories', CategoryController.getCategories);
router.get('/categories/:type', CategoryController.getCategoriesByType);
router.get('/categories/:type/:id', CategoryController.getCategory);
router.post('/categories', CategoryController.postCategory);
router.put('/categories/:id', CategoryController.putCategory);
router.delete('/categories/:id', CategoryController.deleteCategory);

/* TALE*/
router.get('/tales', TaleController.getTales);
router.get('/tales/:id', TaleController.getTale);
// router.post('/tale', TaleController.postTale);
// router.put('/tale/:id', TaleController.putTale);
// router.delete('/tale/:id', TaleController.deleteTale);

/* VERB */
router.get('/verbs', VerbController.getVerbs);
router.get('/verbs/:id', VerbController.getVerb);
// router.post('/verb', VerbController.postVerb);
// router.put('/verb/:id', VerbController.putVerb);
// router.delete('/verb/:id', VerbController.deleteVerb);

/* EXAMPLE */
  router.get('/examples', ExampleController.getExamples);
  router.get('/examples/:id', ExampleController.getExample);
  router.get('/categories/:id/examples', ExampleController.getExampleByCategory);

/* VERB EXAMPLE */
router.get('/verb-examples', VerbExampleController.getVerbExamples);
router.get('/verbs/:id/verb-examples', VerbExampleController.getAllVerbExamplesFromAVerb);
router.get('/verb-examples/:id', VerbExampleController.getVerbExampleById);

/* USERS */
router.get('/user', userController.getAllUsers);
router.post('/user', userController.registerNewUser);
router.post('/login', userController.serachUserByEmail);


// router.route('/:postId')
//     .get(getPost)
//     .delete(deletePost)
//     .put(updatePost);

export {router as RouterAPI};