export class Verb {
    id: number;
    verbQuechua: string;
    verbSpanish: string;
    imageURL !: string;
    time !: string;

    constructor(id: number, verbQuechua: string, verbSpanish: string){
        this.id = id;
        this.verbQuechua = verbQuechua;
        this.verbSpanish = verbSpanish;
    }

    setData(imageURL: string, time: string){
        this.imageURL = imageURL;
        this.time = time;
    }
}