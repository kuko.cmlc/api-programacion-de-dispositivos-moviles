export class VerbExample{
    id: number;
    verbId: number;
    sentenceQuechua !: string;
    sentenceSpanish !: string;
    time: string;

    constructor(id: number, verbId: number, time: string){
        this.id = id;
        this.verbId = verbId;
        this.time = time;
    }

    setSentences(sentenceQuechua: string, sentenceSpanish: string){
        this.sentenceQuechua = sentenceQuechua;
        this.sentenceSpanish = sentenceSpanish;
    }

}