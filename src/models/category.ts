export class Category{

    id: number;
    categoryQuechua: string;
    categorySpanish: string;
    type: string;

    constructor(id:number, categoryQuechua: string, categorySpanish:string, type:string){
        this.id = id;
        this.categoryQuechua = categoryQuechua;
        this.categorySpanish = categorySpanish;
        this.type = type;
    }

}