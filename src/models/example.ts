export class Example {
    id: number;
    categoryId: number;
    sentenceQuechua !: string;
    sentenceSpanish !: string;
    type: string;

    constructor(id: number, categoryId: number,sentenceQuechua: string, sentenceSpanish: string, type: string){
        this.id = id;
        this.categoryId = categoryId;
        this.sentenceQuechua = sentenceQuechua;
        this.sentenceSpanish = sentenceSpanish;
        this.type = type;
    }

    setSentences(sentenceQuechua: string, sentenceSpanish: string){
        this.sentenceQuechua = sentenceQuechua;
        this.sentenceSpanish = sentenceSpanish;
    }
}