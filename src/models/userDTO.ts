import crypto from "crypto";
class userDTO {
  id: String;
  name: String;
  lastName: String;
  email: String;
  password: String;
  salt: String;

  constructor(educationModelDTO: any) {
    this.id = educationModelDTO._id?.toString();
    this.name = educationModelDTO.name;
    this.lastName = educationModelDTO.lastName;
    this.email = educationModelDTO.email.toLowerCase();
    this.salt = this.setSalt();
    this.password = this.setPassword(educationModelDTO.password);
  }

  setSalt() {
    if (this.salt != null || this.salt != undefined) {
      return this.salt;
    } else {
      return crypto.randomBytes(16).toString("hex");
    }
  }

  setPassword(password: any) {
    if (password == null || password == undefined) {
      return this.password.toString();
    } else {
      return crypto
        .pbkdf2Sync(password, this.salt.toString(), 1000, 64, `sha512`)
        .toString(`hex`);
    }
  }
}
export default userDTO;
