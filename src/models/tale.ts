export class Tale {

    id: number;
    titleQuechua !: string;
    titleSpanish !: string;
    author !: string;
    contentQuechua !: string;
    contentSpanish !: string;
    imageURL !: string;
    type: string;
    section: string;

    constructor(id: number, type: string, section: string){
        this.id = id;
        this.type = type;
        this.section = section;
    }

    setTitles(titleQuechua: string, titleSpanish: string, author: string){
        this.titleQuechua = titleQuechua;
        this.titleSpanish = titleSpanish;
        this.author = author;
    }

    setContentInfo(contentQuechua: string, contentSpanish: string, imageURL: string){
        this.contentQuechua = contentQuechua;
        this.contentSpanish = contentSpanish;
        this.imageURL = imageURL;
    }

}