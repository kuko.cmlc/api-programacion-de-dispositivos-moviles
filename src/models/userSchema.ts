import { Schema, model } from "mongoose";

const user = new Schema(
  { 
    name: { type: String, required: true },
    lastName: { type: String, required: true },
    email: { type: String, unique : true, required: true},
    password: { type: String, required: true },
    salt: { type: String, required: true },
  },
  {
    timestamps: true,
    versionKey: false,
  }
);

export default model('user', user);