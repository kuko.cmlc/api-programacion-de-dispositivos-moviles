import { Example } from "../models/example";
import examplesMock from "../mock-data/examplesMock"

let data: Array<Example> = new Array<Example>();
export default class ExampleService{
    constructor(){
        this.getMockData()
    }

    getMockData () {
        data = examplesMock
    }

    getAllExamples(): Array<Example>{
        return data;
    }

    getExampleById(id: number): Array<Example> {
        return data.filter(t => t.categoryId == id);
    }

    getExamplesByCategory(categoryId: number): Array<Example>{
        console.log("MOCK DATA INTEGRATION")
        return data.filter(t => t.categoryId == categoryId);
    }
}
