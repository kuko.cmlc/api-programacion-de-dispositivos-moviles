import { Verb } from "../models/verb";

let data: Array<Verb> = new Array<Verb>();
export default class VerbService {
    constructor(){
        let verb1 = new Verb(1, "be", "ser");
        verb1.setData("url", "present");

        let verb2 = new Verb(2, "help", "ayudar");
        verb2.setData("url", "present");

        data.push(verb1);
        data.push(verb2);
    }

    getAllVerbs(): Array<Verb> {
        return data;
    }

    getVerbById(id: number): Verb {
        return data.filter(v => v.id == id)[0];
    }

    getVerbsByTime(time: string): Array<Verb> {
        return data.filter(v=> v.time == time);
    }
}

