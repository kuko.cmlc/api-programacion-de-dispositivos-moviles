import userSchema from "../models/userSchema";
import userDTO from "../models/userDTO";
import userRepository from "../repository/userRepository";
import crypto  from 'crypto'
class UserService {
  userRepository: userRepository;
  constructor() {
    this.userRepository = new userRepository();
  }

  async getAllUsers() {
    const queryResult = await this.userRepository.getAllusers();
    return queryResult;
  }

  async registerNewUser(newUser: any) {
    try {
      let responseDTO = new userDTO(newUser);
      const newUserSchema = new userSchema(responseDTO);
      await this.userRepository.registerNewUser(newUserSchema);
      return responseDTO;
    } catch (error) {
      return error
    }
  }

  async searchUserByEmail(loginUser: any) {
    const email = loginUser.email.toLowerCase();
    let resultResponse = null;
    let queryResult = await this.userRepository.searchUserByEmail(email);
    if (queryResult[0] != null && queryResult[0] != undefined){
      const result = this.checkPasswordDB(loginUser.password, queryResult[0])
      if (result){
        resultResponse = this.createUserResponse( queryResult[0].name,queryResult[0].lastName,queryResult[0].email,result)
      }
      else {
        resultResponse = this.createUserResponse( "Error Login", "", queryResult[0].email,result)
      }
    }
    else{
      resultResponse = this.createUserResponse( "User not Found", "",  loginUser.email,false)
    }
    return resultResponse;
  }

  checkPasswordDB (password: any, queryResult: any){
    var hash = crypto.pbkdf2Sync(password, queryResult.salt, 1000, 64, `sha512`).toString(`hex`);
    return queryResult.password === hash;
  }

  createUserResponse(name: any, lastName:any, email: any, isLogin: any){
    const userResponse = {
      fullName: name + " " + lastName,
      email: email,
      isLogin: isLogin
    }
    return userResponse;
  }
}

export default UserService;
