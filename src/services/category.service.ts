import { Category } from "../models/category";
import categoriesMock from "../mock-data/categoriesMock"

let data: Array<Category> = new Array<Category>();
export default class CategoryService {
  constructor() {
    this.getMockData()
  }

  getMockData () {
    data = categoriesMock
  }

  getAllCategories(): Array<Category> {
    return data;
  }

  getCategoryById(id: number, type: String): Category {
    return data.filter((c) => ((c.id == id) && (c.type == type)))[0];
  }

  addCategory(category: Category): void {
    let nextId = data.length == 0 ? 1 : data[data.length - 1].id + 1;
    category.id = nextId;
    data.push(category);
  }

  updateCategory(category: Category, id: number): void {
    const idToUpdate = data.filter((c) => c.id == id)[0];
    idToUpdate.categoryQuechua = category.categoryQuechua;
    idToUpdate.categorySpanish = category.categorySpanish;
    idToUpdate.type = category.type;
  }

  deleteCategory(id: number): void {
    const idToDelete = data.filter((c) => c.id == id)[0].id - 1;
    data.splice(idToDelete);
  }

  getCategoruesByType(type: String): Array<Category> {
    return data.filter((c) => c.type == type);
  }
}
