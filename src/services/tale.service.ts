import { Tale } from "../models/tale";
import talesMock from "../mock-data/talesMock"
 

let data: Array<Tale> = new Array<Tale>();
export default class TaleService {
    
    constructor(){
        this.createTales();
    }

    createTales() { 
        data = talesMock;
    }

    getAllTales(): Array<Tale> {
        return data;
    }

    getTaleById(id: number): Tale{
        return data.filter(t => t.id == id)[0];
    }
}