import { VerbExample } from "../models/verbExample";

let data: Array<VerbExample> = new Array<VerbExample>();
export default class VerbExampleService{
    constructor(){
        let verbExample1 = new VerbExample(1,1,"present");
        verbExample1.setSentences('this is sentence', 'esta es una oracion');
        let verbExample2 = new VerbExample(2,2,"present");
        verbExample2.setSentences('this is sentence 2', 'esta es una oracion 2');

        data.push(verbExample1);
        data.push(verbExample2);
    }

    getAllVerbExamples(): Array<VerbExample> {
        return data;
    }

    getAllVerbExamplesFromAVerb(verbId: number): Array<VerbExample> {
        return data.filter(ve=> ve.verbId == verbId);
    }

    getVerbExampleById(id: number): VerbExample {
        return data.filter(ve => ve.id == id)[0];
    }
}