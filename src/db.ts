import mongoose from "mongoose";

const DataBaseConnection = "mongodb://allpaqa:allpaqa123@cluster0-shard-00-00.uhonm.mongodb.net:27017,cluster0-shard-00-01.uhonm.mongodb.net:27017,cluster0-shard-00-02.uhonm.mongodb.net:27017/myFirstDatabase?ssl=true&replicaSet=atlas-usbbsa-shard-0&authSource=admin&retryWrites=true&w=majority"
const connectDB = async () => {
  try {
    await mongoose.connect(DataBaseConnection, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useFindAndModify: false,
      useCreateIndex: true,
    });    
    console.log("Mongodb is connected!!" , process.env.PORT);
  } catch (error) {
    console.error(error); 
  }
}

export default connectDB;