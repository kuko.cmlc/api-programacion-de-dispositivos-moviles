import userSchema from "../models/userSchema";

class userRepository {
  async getAllusers() {
    return await userSchema.find();
  }

  async registerNewUser(newUser: any) {
    try {
      await newUser.save();
    } catch (error) {
      throw new Error(error);
    }
  }

  async searchUserByEmail (email: String) {
    return await userSchema.find({ email : email });
  }

}

export default userRepository