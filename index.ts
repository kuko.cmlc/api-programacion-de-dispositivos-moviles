import express from 'express';
import { RouterAPI } from './src/routes';
import conectDB from './src/db'

var bodyParser = require('body-parser');


const app = express();
const PORT = 8000;
app.use(bodyParser.urlencoded({extended: false}));
app.use(express.json());
app.use('/api', RouterAPI);

conectDB().then(resp => {
  app.listen(process.env.PORT || PORT, () => {
    console.log(`Server is running at https://localhost:${PORT}`);
  });
});
